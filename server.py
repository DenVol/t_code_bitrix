from flask import Flask, render_template, request, jsonify
from bitrix_api import make_report

PORT = 20207

app = Flask(__name__)
app.debug = True

@app.route('/')
def index():
    return render_template("index.html")

@app.route('/create', methods=['POST'])
def create():
    start_date = request.json['start_date']
    end_date = request.json['end_date']

    url = make_report(start_date, end_date)
    return jsonify({'url': url})

if __name__ == "__main__":
    app.run(port=PORT)