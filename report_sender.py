import requests

YANDEX_DISK = "https://cloud-api.yandex.net/v1/disk"
YANDEX_TOKEN = ""
header = {
        "Authorization": YANDEX_TOKEN,
        "Content-Type": "application/json",
        "Accept": "application/json"
    }

def send_report(filename: str, start_date: str, end_date: str) -> str:

    dirname = f"reports_dir {start_date} - {end_date}"

    check_dir_exist = requests.get(url=f"{YANDEX_DISK}/resources?path={dirname}", headers=header)

    if check_dir_exist.status_code == 404:
        create_dir = requests.put(url=f"{YANDEX_DISK}/resources?path={dirname}", headers=header)
        if create_dir.status_code != 201:
            raise Exception(create_dir.content)
    elif check_dir_exist.status_code != 200:
        raise Exception(check_dir_exist.content)
    
    check_file_exist = requests.get(url=f"{YANDEX_DISK}/resources?path={dirname}%2F{filename}", headers=header)
    if check_file_exist.status_code != 404:
        requests.delete(url=f"{YANDEX_DISK}/resources?path={dirname}%2F{filename}", headers=header)

    get_download_url = requests.get(url=f"{YANDEX_DISK}/resources/upload?path={dirname}%2F{filename}", headers=header)
    if get_download_url.status_code != 200:
        raise Exception(get_download_url.content)
        
    upload_url = get_download_url.json()["href"]

    with open(filename, 'rb') as f:
        upload_file = requests.put(url=upload_url, files={filename: f}, headers=header)
        if upload_file.status_code != 201:
            raise Exception(upload_file.content)
    
    make_publish = requests.put(url=f"{YANDEX_DISK}/resources/publish?path={dirname}%2F", headers=header)
    if make_publish.status_code != 200:
        raise Exception(make_publish.content)
    
    make_publish_file = requests.put(url=f"{YANDEX_DISK}/resources/publish?path={dirname}%2F{filename}", headers=header)
    if make_publish_file.status_code != 200:
        raise Exception(make_publish_file.content)

    publish_urls = requests.get(url=f"{YANDEX_DISK}/resources/public", headers=header)

    for item in publish_urls.json()["items"]:
        if item["name"] == dirname:
            return item["public_url"]

    raise Exception(publish_urls.content)
