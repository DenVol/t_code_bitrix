import xlsxwriter
import time
import datetime

def create_excel(start_date: str, end_date:str, group_info: dict, user_info:dict, all_tasks_id: list) -> str:
    try:
        filename = f"Отчёт с {start_date} по {end_date}.xlsx"
        worbook = xlsxwriter.Workbook(filename)
        bold = worbook.add_format({'bold': True})
        time_format = worbook.add_format({"num_format": "hh:mm:ss"})
        
        worksheet = worbook.add_worksheet(name="Report")
        worksheet.set_column("A:G", 45)

        worksheet.write('A1', 'ФИО', bold)
        worksheet.write('B1', 'Задача', bold)
        worksheet.write('C1', 'Проект', bold)
        worksheet.write('D1', 'Потрачено времени', bold)
        worksheet.write('E1', 'Дата', bold)

        row = 1
        column_dict = {
            "user": 0,
            "task": 1,
            "project": 2,
            "time": 3,
            "date": 4,
        }

        for user in user_info.values():
            if "elapsed_list" not in user:
                continue
            elapsed_count = 0
            for elapsed_item in user["elapsed_list"]:
                group_id = None
                for id, group in group_info.items():
                    if elapsed_item["TASK_ID"] in group["tasks"]:
                        group_id = id
                if not group_id or group_id == "0":
                    continue
                
                worksheet.write(row, column_dict["user"], user["username"])

                worksheet.write(row, column_dict["task"], group_info[group_id]["tasks"][elapsed_item["TASK_ID"]])
                worksheet.write(row, column_dict["project"], group_info[group_id]["group_name"])

                task_time = time.gmtime(int(elapsed_item["SECONDS"]))
                worksheet.write_datetime(row, column_dict["time"], datetime.time(*(task_time)[3:6]), time_format)

                worksheet.write(row, column_dict["date"], elapsed_item["CREATED_DATE"][:10])

                row += 1
                elapsed_count += 1           

            if elapsed_count > 0:
                worksheet.write(row, 0, "Суммарно затраченное время", bold)
                worksheet.write_formula(row, 1, f"=SUM(D{row - elapsed_count + 1}:D{row})", time_format, "")
                row += 1
        
    except Exception as ex:
        print(ex)
    finally:
        worbook.close()
        return filename
