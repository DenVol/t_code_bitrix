function generate_report() {
    var start_date_value = document.getElementById("start_date").value;
    var end_date_value = document.getElementById("end_date").value;

    if (start_date_value === "" || end_date_value === "") {
        alert("Вы не выбрали даты");
        return
    }
    
    const start_date = new Date(start_date_value);
    const end_date = new Date(end_date_value);
    
    if (end_date - start_date < 0) {
        alert("Дата окончания не может наступить раньше даты начала")
        return
    }

    const data = {
        "start_date": start_date_value,
        "end_date": end_date_value
    }
    document.getElementById("spinnerBox").style["display"] = "flex";
    document.querySelector("#execute_button").disabled = true;

    fetch("/create", {
        method: "POST",
        headers: {
                "Content-Type": "application/json"
        },
        body: JSON.stringify(data)
    }).then((responce) =>
        responce.json()
    ).then((data) => {
        let link_place = document.querySelector("#yandex_url")
        link_place.innerHTML = "Ссылка на отчёт: " + data.url
        link_place.href = data.url
    }).catch((error) => {
        console.log("Error: ", error)
        let link_place = document.querySelector("#yandex_url")
		link_place.innerHTML = "Произошла ошибка: " + error
    }).finally(() => {
        document.getElementById("spinnerBox").style["display"] = "none";
        document.querySelector("#execute_button").disabled = false;
    })

}