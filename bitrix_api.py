import os
import requests

from report_creator import create_excel
from report_sender import send_report

BASE_URL = ""

def get_groups(group_info: dict) -> None:
    groups = list(group_info.keys())
    for group in groups:
        if group == '0':
            continue
        data = {
            "ORDER": {"ID": "ASC"},
            "FILTER": {
                "%ID": group
            }
        }
        response = requests.post(url=f"{BASE_URL}/sonet_group.get.json", json=data).json()["result"]
        group_info[response[0]["ID"]]["group_name"] = response[0]['NAME']

def get_elapseditem(start_datetime: str, end_datetime: str) -> None:
    data = [
        {
            "ID": "asc"
        },
        {
            ">=CREATED_DATE": start_datetime,
            "<=CREATED_DATE": end_datetime,
        },
        ["*"],
        {
            "NAV_PARAMS": {
                "nPageSize": 50,
                "iNumPage": 1
            } 
        }
    ]
    response = requests.post(url=f"{BASE_URL}/task.elapseditem.getlist", json=data).json()
    return get_batch("task.elapseditem.getlist", response['total'], data)

def get_user_info(all_users_id: list, user_info: dict) -> None:
    data = {
        "halt": 0,
        "cmd": {}
    }

    batch_response = dict()

    for i, user_id in enumerate(all_users_id):
        data["cmd"][user_id] = f"user.get?&FILTER[ID]={user_id}"
        if len(data["cmd"]) == 50 or i + 1 >= len(all_users_id):
            batch_response.update(requests.post(url=f"{BASE_URL}/batch", json=data).json()["result"]["result"])
            data["cmd"] = dict()
    
    for user_id in user_info:
        user_info[user_id]["username"] = f"{batch_response[user_id][0]['NAME']} {batch_response[user_id][0]['LAST_NAME']}"


def get_task_info(all_tasks_id: list, group_info: dict) -> None:
    
    data = {
        "halt": 0,
        "cmd": {}
    }

    batch_response = {}
    all_tasks_id = [int(i) for i in all_tasks_id]
    min_id = min(all_tasks_id)
    max_id = max(all_tasks_id)

    data_first = {
        "filter": {
            ">ID": min_id,
            "<ID": max_id
        },
        "select": ["ID", "TITLE", "GROUP_ID"]
    }

    first_response = requests.post(url=f"{BASE_URL}/tasks.task.list", json=data_first).json()
    total = first_response["total"]

    batch_response = dict()
    for i in range(total // 50 + 2):
        data["cmd"][f"task_result_{i}"] = f"tasks.task.list?&filter[<id]={max_id}&filter[>id]={min_id}&order[id]=asc&start={i * 50}&select[]=ID&select[]=TITLE&select[]=GROUP_ID"
        if len(data["cmd"]) == 50 or i * 50 >= total:
            response = requests.post(url=f"{BASE_URL}/batch", json=data)
            batch_response.update(response.json()["result"]["result"])
            data["cmd"] = dict()


    for task_result in batch_response.values():
        for task in task_result['tasks']:
            if task['groupId'] in group_info:
                group_info[task['groupId']]['tasks'][task['id']] = task['title']
            else:
                group_info[task['groupId']] = {'tasks' : {
                            task['id']: task['title']
                        }
                }


def get_batch(request, total, filter: list):
    data = {
        "halt": 0,
        "cmd": {}
    }

    total = int(total)
    batch_response = dict()

    for i in range(total // 50 + 2):
        data["cmd"][f"elapsed_item_{i}"] = f"{request}?&ORDER[ID]=asc&FILTER[>CREATED_DATE]={filter[1]['>=CREATED_DATE']}&FILTER[<CREATED_DATE]={filter[1]['<=CREATED_DATE']}&SELECT[]=TASK_ID&SELECT[]=SECONDS&SELECT[]=CREATED_DATE&SELECT[]=USER_ID&PARAMS[NAV_PARAMS][iNumPage]={filter[3]['NAV_PARAMS']['iNumPage']}"
        filter[3]["NAV_PARAMS"]["iNumPage"] += 1
        if len(data["cmd"]) == 50 or i * 50 >= total:
            batch_response.update(requests.post(url=f"{BASE_URL}/batch", json=data).json()["result"]["result"])
            data["cmd"] = dict()
    
    elapsed_list = list()
    for batch_element in batch_response.values():
        elapsed_list.extend(batch_element)
    
    return elapsed_list  


def make_report(start_date: str, end_date: str) -> str:

    elapsed_list = get_elapseditem(start_date, end_date)

    all_tasks_id = list()
    all_users_id = list()
    for elapsed_item in elapsed_list:
        if elapsed_item['TASK_ID'] not in all_tasks_id:
            all_tasks_id.append(elapsed_item['TASK_ID'])
        if elapsed_item['USER_ID'] not in all_users_id:
            all_users_id.append(elapsed_item['USER_ID'])
            
    group_info = dict()
    get_task_info(all_tasks_id, group_info)

    get_groups(group_info)
    
    user_info = {user: {} for user in all_users_id}
    get_user_info(all_users_id, user_info)

    for elapsed_item in elapsed_list:
        if "elapsed_list" not in user_info[elapsed_item['USER_ID']]:
            user_info[elapsed_item['USER_ID']]["elapsed_list"] = [elapsed_item,]
        elif "elapsed_list" in user_info[elapsed_item['USER_ID']]:
            user_info[elapsed_item['USER_ID']]["elapsed_list"].append(elapsed_item)

    filename = create_excel(start_date, end_date, group_info, user_info, all_tasks_id)    

    url = send_report(filename, start_date, end_date)
    os.remove(filename)
    return url